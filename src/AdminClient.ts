import { resolve } from 'path';
import { chat, chatTypeSupergroup } from 'tdl/types/tdlib';
import { Inject, Singleton } from 'typescript-ioc';

import { Group, GroupRepository } from '@entities/Group';
import { Settings, SettingsRepository } from '@entities/Settings';

import { BaseTelegramClient } from './core/BaseTelegramClient';

@Singleton
export class AdminClient extends BaseTelegramClient {

    constructor(
        @Inject protected readonly grep: GroupRepository,
        @Inject protected readonly coreSettings: Settings,
        @Inject protected readonly coreSettingsRepository: SettingsRepository
    ) {
        super();
    }

    async reinit(botName: string): Promise<void> {
        const chat = await this.getBotChat(botName);
        const botId = chat.id;
        try {
            const message = await this.sendMarkdown(chat.id, '/start noecho');
            await this.client.invoke({
                _: 'deleteMessages',
                chat_id: chat.id,
                message_ids: [message.id]
            });
        } catch (err) {
            LOG_ERROR(err);
        }

        let c: chat;
        try {
            if (!this.coreSettings.system_supergroup_id) {
                throw 'system_supergroup_id required';
            }
            const { title, id } = c = await this.getSupergroupChat(this.coreSettings.system_supergroup_id);
            const { status } = await this.client.invoke({
                _: 'getChatMember',
                user_id: this._id,
                chat_id: id
            });
            if (status._ !== 'chatMemberStatusCreator' || !status.is_member) {
                throw `Not creator in ${title}:${this.coreSettings.system_supergroup_id} required`;
            }
            await this.addAdminToSupergroup(botId, id);
        } catch (err) {
            this.coreSettings.system_supergroup_id = 0;
            LOG_WARN(err);
            c = await this.createSupergroupIfNotExists('RU Initiative support', `Группа куда следует поместить администраторов поддержки.\nНЕ ПЕРЕИМЕНОВЫВАТЬ.\nЗаявки пользователей удаляются после первого ответа, либо по нажатию на "Игнорировать".`, false);
            await Promise.all([
                this.setChatPhoto(c.id, resolve(__dirname + '/../chat_photo.png')),
                this.addAdminToSupergroup(botId, c.id)
            ]);
            this.coreSettings.system_supergroup_id = (c.type as chatTypeSupergroup).supergroup_id;
            await this.coreSettingsRepository.save(this.coreSettings);
        }
        await this.pingChats([c]);
    }

    async getExtChatInfo(chat_id: number): Promise<{ c: number, m?: number[] }> {
        let chat = await this.client.invoke({ _: 'getChat', chat_id });
        if (chat.client_data) {
            return JSON.parse(chat.client_data);
        }
        const { supergroup_id } = (chat.type as chatTypeSupergroup);
        const { description } = await this.client.invoke({
            _: 'getSupergroupFullInfo',
            supergroup_id
        });
        return JSON.parse(description);
    }

    async updateExtChatInfo(chat_id: number, source_chat_id: number, messages_ids?: number[]): Promise<any> {
        const client_data = JSON.stringify({ c: source_chat_id, m: messages_ids });
        return this.client.invoke({ _: 'setChatClientData', chat_id, client_data });
    }

    async createGroup(sourceChat: chat, botId: number, invite_message_id: number): Promise<{ invite_message_id: number, group: Group, chat: chat }> {
        const { title } = sourceChat;
        const client_data = JSON.stringify({ c: sourceChat.id, m: [invite_message_id] });
        const chat = await this.client.invoke({
            _: 'createNewSupergroupChat',
            title: '🤖' + title.substr(0, 120),
            is_channel: false,
            description: client_data,
        });
        const [{ invite_link }] = await Promise.all([
            this.client.invoke({ _: 'generateChatInviteLink', chat_id: chat.id, }),
            this.client.invoke({ _: 'addChatMember', chat_id: chat.id, user_id: botId }),
            this.client.invoke({ _: 'setChatClientData', chat_id: chat.id, client_data }),
        ]);
        await this.client.invoke({
            _: 'setChatMemberStatus',
            chat_id: chat.id,
            user_id: botId,
            status: {
                _: 'chatMemberStatusAdministrator',
                can_be_edited: true,
                can_change_info: true,
                can_delete_messages: true,
                can_invite_users: true,
                can_restrict_members: true,
                can_pin_messages: true,
                can_promote_members: true
            }
        });
        const { supergroup_id } = chat.type as chatTypeSupergroup;
        const group = new Group();
        group.userId = sourceChat.id;
        group.supergroupId = supergroup_id;
        group.comment = invite_link ? 'tg://join?invite=' + invite_link.split('/', 5)[4] + '|' + title : '#|' + title;
        await this.grep.save(group);
        return { group, chat, invite_message_id };
    }

    deleteSupergroup(supergroup_id: number): Promise<any> {
        return this.client.invoke({ _: 'deleteSupergroup', supergroup_id });
    }
}