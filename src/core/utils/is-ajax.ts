import { Context } from 'koa';

export default function (ctx: Context) {
    return ctx.request.headers['x-requested-with'] === 'XMLHttpRequest';
}