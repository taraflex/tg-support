import { Column, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { SingletonEntity } from '@ioc';

@Access({ GET: 0, PATCH: 0 }, { display: 'single', icon: 'robot', order: 5 })
@SingletonEntity()
export class BotSettings {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ empty: false, description: 'Bot name - имя в списке чатов' })
    @Column({ default: 'Russian Initiative' })
    botname: string;

    @v({ empty: false, description: 'People will see this description when they open a chat with your bot, in a block titled "What can this bot do?".' })
    @Column({ default: 'Бот поддержки myfri.org' })
    description: string;

    @v({ empty: false, description: 'People will see this text on the bot\'s profile page and it will be sent together with a link to your bot when they share it with someone.' })
    @Column({ default: 'Счастье Родины в твоих руках' })
    about: string;
}

export abstract class BotSettingsRepository extends Repository<BotSettings>{ }