import Backoff from 'backo2';
import { IDisposable, Terminal } from 'xterm';

import { notify } from '@frontend/error-handler-mixin';
import { TAB_ID } from '@utils/axios-smart';
import delay from '@utils/delay';
import { MultiChain } from '@utils/MultiChain';

import { RPC } from './RPC';

declare module 'vue/types/vue' {
    interface Vue {
        $rpc: ClientRPC;
    }
}
// todo waitConnect before any remote call
export class ClientRPC extends RPC {
    ws: WebSocket;
    termListeners: IDisposable[] = [];
    protected readonly url = location.origin.replace(/^http/i, 'ws') + '/ws/';
    protected readonly backoff = new Backoff({ min: 1000, max: 60 * 1000 });
    protected readonly chain = new MultiChain<true>();
    constructor() {
        super();
        this.restart();
    }
    readonly restart = async () => {
        this.dispose();
        const d = this.backoff.duration();
        if (d > 1000) {
            await delay(d);
        }
        if (this.term) {
            const { cols, rows } = this.term as Terminal;
            this.ws = new WebSocket(this.url + `?cols=${cols}&rows=${rows}&tab=${TAB_ID}`);
        } else {
            this.ws = new WebSocket(this.url + '?tab=' + TAB_ID);
        }
        this.ws.binaryType = 'arraybuffer';
        this.ws.onerror = this.onerror;
        this.ws.onclose = this.restart;
        this.ws.onmessage = this.onmessage;
        this.ws.onopen = this.onopen;
    }
    protected readonly onerror = () => {
        notify(navigator.onLine === false ? 'Connection error. Posible offline.' : 'Connection error');
        this.restartIfClosed();
    }
    protected readonly onopen = () => {
        this.backoff.reset();
        this.chain.write(true);
    }
    async waitConnect() {
        while (!this.ws || this.ws.readyState !== 1 /*WebSocket.OPEN*/) {
            await this.chain.read();
        }
    }
    async enableTerminal(term: Terminal) {
        await this.waitConnect();
        const { searchParams } = new URL(this.ws.url);
        if (!searchParams.has('cols') || !searchParams.has('rows')) {
            await this.remote<RPC>('RPC', 30).enableTerminal({ cols: term.cols, rows: term.rows });
        }
        this.termListeners.forEach(l => l.dispose());
        this.termListeners.push(term.onData(data => {
            this.ws && this.ws.send(data);
        }));
        this.termListeners.push(term.onResize(size => {
            !this.isClosed && this.remote<RPC>('RPC', 30).enableTerminal(size).catch(notify);
        }));
        this.term = term;
    }
    protected readonly onmessage = (message: MessageEvent) => {
        this.process(message.data).catch(notify);
    }
    protected rcall(o: any, fname: string, args: any[]) {
        return o[fname](...args);
    }
    protected send(data: Uint8Array) {
        //todo await this.waitConnect();
        this.ws.send(data);
    }
    get isClosed() {
        return !this.ws || this.ws.readyState === 3/* WebSocket.CLOSED*/ || this.ws.readyState === 2 /*WebSocket.CLOSING*/;
    }
    restartIfClosed() {
        if (this.isClosed) {
            this.restart();
        }
    }
    dispose() {
        if (this.ws) {
            this.ws.onclose = undefined;
            this.ws.onerror = undefined;
            this.ws.onopen = undefined;
            this.ws.onmessage = undefined;
            this.ws.close();
            this.ws = null;
        }
    }
}