import { AdminClient } from 'src/AdminClient';
import { BotClient } from 'src/BotClient';
import { EntitySubscriberInterface, EventSubscriber, UpdateEvent } from 'typeorm';
import { Container } from 'typescript-ioc';

import { BotSettings } from '@entities/BotSettings';
import delay from '@utils/delay';

@EventSubscriber()
export class BotSettingsSubscriber implements EntitySubscriberInterface<BotSettings> {

    listenTo() {
        return BotSettings;
    }

    async beforeUpdate({ entity }: UpdateEvent<BotSettings>) {
        const admin = (Container.get(AdminClient) as AdminClient);
        const bot = (Container.get(BotClient) as BotClient)

        const botInfo = await bot.getMe();
        if (botInfo && admin.id) {
            let id = 0;
            let needwait = false;
            if (entity.description) {
                id = id || (await admin.getFatherChat()).id;
                await admin.sendMarkdown(id, '/setdescription')
                await delay(1000);
                await admin.sendPlainText(id, '@' + botInfo.username)
                await delay(1000);
                await admin.sendMarkdown(id, entity.description);
                needwait = true;
            }
            if (entity.about) {
                if (needwait) {
                    await delay(1000);
                }
                id = id || (await admin.getFatherChat()).id;
                await admin.sendMarkdown(id, '/setabouttext')
                await delay(1000);
                await admin.sendPlainText(id, '@' + botInfo.username)
                await delay(1000);
                await admin.sendMarkdown(id, entity.about);
                needwait = true;
            }
            if (entity.botname) {
                if (needwait) {
                    await delay(1000);
                }
                id = id || (await admin.getFatherChat()).id;
                await admin.sendMarkdown(id, '/setname')
                await delay(1000);
                await admin.sendPlainText(id, '@' + botInfo.username)
                await delay(1000);
                await admin.sendPlainText(id, entity.botname);
                needwait = true;
            }
        }
    }
}