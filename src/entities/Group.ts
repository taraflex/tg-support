import { Column, Entity, PrimaryColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { RTTIItemState } from '@crud/types';

@Access({ GET: 0, DELETE: 0 }, { display: 'table', icon: 'bullhorn', order: 10 })
@Entity()
export class Group {
    @v({ state: RTTIItemState.HIDDEN })
    @PrimaryColumn({ type: 'bigint' })
    userId: number;

    @v({ type: 'link' })
    @Column({ default: '' })
    comment: string;

    @v({ state: RTTIItemState.HIDDEN })
    @Column({ type: 'bigint', unique: true })
    supergroupId: number;
}

export abstract class GroupRepository extends Repository<Group>{ }