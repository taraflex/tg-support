import { execSync } from 'child_process';
import { IPty, IPtyForkOptions } from 'node-pty';
import { platform } from 'os';

import { PORT } from '@utils/config';

function hasTmux() {
    try {
        execSync('command -v tmux', { windowsHide: true });
        return true;
    } catch {
        return false;
    }
}

const shell = platform().includes('win') ? 'cmd.exe' : (hasTmux() ? 'tmux' : 'sh');

let spawn = null;
try {
    spawn = require('node-pty').spawn;
} catch { }

export const AVALIBLE = !!spawn;

export function create(options: IPtyForkOptions): IPty {
    return AVALIBLE ? spawn(
        shell,
        shell === 'tmux' ? ['new', '-A', '-s', APP_NAME + '-' + PORT] : [],
        {
            name: 'xterm-color',
            cwd: process.env.HOME,
            env: process.env,
            ...options
        }
    ) : null;
}