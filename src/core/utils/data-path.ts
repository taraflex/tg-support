import AppDirectory from 'appdirectory';
import { sync as mkDirSync } from 'make-dir';
import { join, normalize } from 'path';

import { PORT } from '@utils/config';

let cwd = process.cwd();

function createDataDir(path: string): string {
    try {
        mkDirSync(path);
        return path;
    } catch (err) {
        LOG_ERROR(err);
        if (path.startsWith(cwd)) {
            path = path.slice(cwd.length);
            cwd = join(cwd, './../');
            return createDataDir(join(cwd, '/', path));
        } else {
            return path;
        }
    }
}

const path = createDataDir(normalize(new AppDirectory({ appName: APP_NAME + '-' + PORT, useRoaming: true }).userData()));

console.log('App directory: ' + path);

export default function (...pathParts: string[]): string {
    return pathParts.length > 0 ? join(path, '/', ...pathParts) : path
}