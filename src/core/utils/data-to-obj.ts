const decoder = TextDecoder && new TextDecoder('utf-8');

export default (ab: ArrayBuffer | string) => new Promise((resolve, reject) => {
    if (ab instanceof ArrayBuffer) {
        if (decoder) {
            const s = decoder.decode(ab);
            try {
                resolve(JSON.parse(s));
            } catch {
                resolve(s);
            }
        } else {
            const fr = new FileReader();
            fr.onloadend = () => {
                if (fr.error) {
                    reject(fr.error);
                } else {
                    try {
                        resolve(JSON.parse(fr.result as string))
                    } catch {
                        resolve(fr.result as string)
                    }
                }
            }
            fr.readAsText(new Blob([new Uint8Array(ab)]));
        }
    } else {
        try {
            if (ab.constructor !== String) {
                throw null;
            }
            resolve(JSON.parse(ab));
        } catch {
            resolve(ab)
        }
    }
});