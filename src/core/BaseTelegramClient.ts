import { ElMessageBox } from 'element-ui/types/message-box';
import { Context } from 'koa';
import pMap from 'p-map';
import sanitize from 'sanitize-filename';
import { Client } from 'tdl';
import { TDLib } from 'tdl-tdlib-ffi';
import {
    chat, formattedText, inputMessageText, message, ReplyMarkup, TdlibParameters, Update
} from 'tdl/types/tdlib';
import { IDisposable } from 'xterm';

import { fromCtx } from '@rpc/ServerRPC';
import { PORT } from '@utils/config';
import dataPath from '@utils/data-path';
import { sendErrorEmail } from '@utils/send-email';
import { API_HASH_RE, BOT_TOKEN_RE, getBotInfo, invisibleLink } from '@utils/tg-utils';

export interface AuthInfo {
    readonly apiHash: string;
    readonly apiId: number;
}

export interface UserAuthInfo extends AuthInfo {
    readonly type: 'user';
    readonly phone: string;
}

export interface BotAuthInfo extends AuthInfo {
    readonly type: 'bot';
    readonly token: string;
}

export abstract class BaseTelegramClient implements IDisposable {
    protected client: Client;

    protected apiHash: string;
    protected apiId: number;
    protected phoneOrToken: string;
    protected _id: number = 0;

    get id() {
        return this._id;
    }

    private readonly tdl = new TDLib('./libtdjson');

    protected readonly tdParams: Partial<TdlibParameters> = {
        _: 'tdlibParameters',
        use_secret_chats: true,
        device_model: APP_NAME + ' ' + PORT,
        enable_storage_optimizer: true,
        use_file_database: true,
        use_chat_info_database: true,
        use_message_database: true,//false,
        ignore_file_names: true
    }

    async searchChats(query: string, limit: number): Promise<chat[]> {
        let chats_ids = new Set((await Promise.all([
            this.client.invoke({ _: 'searchChatsOnServer', query, limit }),
            this.client.invoke({ _: 'searchChats', query, limit })
        ])).flatMap(cs => cs.chat_ids));
        return Promise.all(Array.from(chats_ids, chat_id => this.client.invoke({ _: 'getChat', chat_id })));
    }

    async createSupergroupIfNotExists(title: string, description: string, is_channel?: boolean): Promise<chat> {
        const chats = await this.searchChats(title, 20);
        return chats.find(c => c.title == title && c.type._ == 'chatTypeSupergroup' && c.type.is_channel === !!is_channel) || await this.client.invoke({
            _: 'createNewSupergroupChat',
            title,
            is_channel: !!is_channel,
            description
        });
    }

    async addAdminToSupergroup(user_id: number, chat_id: number) {
        await this.client.invoke({
            _: 'addChatMember',
            user_id,
            chat_id,
        });
        return this.client.invoke({
            _: 'setChatMemberStatus',
            chat_id,
            user_id,
            status: {
                _: 'chatMemberStatusAdministrator',
                can_be_edited: true,
                can_change_info: true,
                can_delete_messages: true,
                can_invite_users: true,
                can_restrict_members: true,
                can_pin_messages: true,
                can_promote_members: true
            }
        });
    }

    async pingChats(chats: readonly chat[], concurrency: number = 6) {
        const input_message_content = this.parseMarkdown(invisibleLink('http://127.0.0.1/'));
        const messages = await pMap(chats, async chat => this.client.invoke({
            _: 'sendMessage',
            chat_id: chat.id,
            disable_notification: true,
            input_message_content
        }), { concurrency });
        return pMap(messages, m => this.client.invoke({ _: 'deleteMessages', message_ids: [m.id], chat_id: m.chat_id }), { concurrency });
    }

    getSupergroupChat(supergroup_id: number): Promise<chat> {
        return this.client.invoke({ _: "createSupergroupChat", supergroup_id });
    }

    setChatPhoto(chat_id: number, path: string) {
        return this.client.invoke({
            _: 'setChatPhoto',
            chat_id,
            photo: { _: 'inputFileLocal', path, }
        });
    }

    parseMarkdown(text: string, disable_web_page_preview?: boolean): inputMessageText {
        return {
            _: "inputMessageText",
            text: this.client.execute({
                _: 'parseTextEntities',
                text,
                parse_mode: { _: 'textParseModeMarkdown' }
            }) as formattedText,
            disable_web_page_preview
        } as inputMessageText;
    }

    sendMarkdown(chat_id: number, text: string, reply_markup?: ReplyMarkup): Promise<message> {
        return this.client.invoke({
            _: 'sendMessage',
            chat_id,
            input_message_content: this.parseMarkdown(text),
            reply_markup
        });
    }

    sendPlainText(chat_id: number, text: string): Promise<message> {
        return this.client.invoke({
            _: 'sendMessage',
            chat_id,
            input_message_content: { _: "inputMessageText", text: { _: 'formattedText', text } }
        });
    }

    async deleteMessages(chat_id: number, ...message_ids: number[]) {
        return this.client.invoke({ _: 'deleteMessages', message_ids, chat_id });
    }

    getFatherChat(): Promise<chat> {
        return this.getBotChat('BotFather');
    }

    async getBotChat(username: string): Promise<chat> {
        const { id } = await this.client.invoke({ _: 'searchPublicChat', username });
        return this.client.invoke({ _: 'createPrivateChat', user_id: id });
    }

    sendErrorEmail() {
        return sendErrorEmail(`Сбой в авторизации телеграм клиента. Пересохраните настройки для получения нового авторизационного кода.`);
    }

    async update(authInfo: Readonly<UserAuthInfo | BotAuthInfo>, ctx?: Context) {
        const { type, apiHash, apiId } = authInfo;
        if (!apiId) {
            throw 'Invalid apiId: ' + apiId;
        }
        if (type != 'user' && type != 'bot') {
            throw 'Invalid tdlib client type: ' + type;
        }
        if (!API_HASH_RE.test(apiHash)) {
            throw 'Invalid apiHash: ' + apiHash;
        }
        if (authInfo.type === 'user' && !authInfo.phone) {
            throw 'Invalid phone: ' + authInfo.phone;
        }
        if (authInfo.type === 'bot' && !BOT_TOKEN_RE.test(authInfo.token)) {
            throw 'Invalid bot token: ' + authInfo.token;
        }
        if (
            !this.client ||
            authInfo.apiHash != this.apiHash ||
            authInfo.apiId != this.apiId ||
            (authInfo.type === 'user' && authInfo.phone != this.phoneOrToken) ||
            (authInfo.type === 'bot' && authInfo.token != this.phoneOrToken)
        ) {
            const phoneOrToken = authInfo.type === 'bot' ? authInfo.token : authInfo.phone;
            const profile = sanitize(type === 'bot' ? (await getBotInfo(phoneOrToken)).id.toString() : phoneOrToken);
            try {
                this.dispose();
                const client = this.client = new Client(this.tdl, {
                    apiId,
                    apiHash,
                    databaseDirectory: dataPath(profile + '/database'),
                    filesDirectory: dataPath(profile + '/files'),
                    verbosityLevel: 1,
                    skipOldUpdates: false,
                    useTestDc: false,
                    useMutableRename: true,
                    tdlibParameters: this.tdParams
                });
                if (this.onUpdate) {
                    client.on('update', async (_: Update) => {
                        try {
                            await (this as any).onUpdate(_);
                        } catch (err) {
                            LOG_ERROR(err);
                        }
                    });
                }
                client.on('error', LOG_ERROR);
                await client.connect();
                await client.invoke({
                    _: 'setOption',
                    name: 'prefer_ipv6',
                    value: { _: 'optionValueBoolean', value: false }
                });
                await client.invoke({
                    _: 'setOption',
                    name: 'online',
                    value: { _: 'optionValueBoolean', value: false }
                });
                this.apiHash = apiHash;
                this.apiId = apiId;
                this.phoneOrToken = phoneOrToken;
                await client.login(() => type === 'bot' ?
                    {
                        type: 'bot',
                        getToken: async () => phoneOrToken
                    } : {
                        type: 'user',
                        getPassword: async (passwordHint) => {
                            const code = await fromCtx(ctx).remote<ElMessageBox>('ElMessageBox').prompt('Введите телеграм пароль.' + (passwordHint ? ' Подсказка: ' + passwordHint : ''), {
                                showCancelButton: false,
                                roundButton: true
                            });
                            return code.value || '';
                        },
                        getPhoneNumber: async _ => phoneOrToken,
                        getAuthCode: async (retry?: boolean) => {
                            if (!ctx) {
                                this.dispose();
                                if (!retry) {
                                    await this.sendErrorEmail();
                                }
                                throw 'Telegram client auth required.';
                            }
                            const code = await fromCtx(ctx).remote<ElMessageBox>('ElMessageBox').prompt('Введите код авторизации телеграм клиента (придет в sms или в уже авторизованный клиент).', {
                                showCancelButton: false,
                                inputType: 'number',
                                roundButton: true
                            });
                            return code.value || '';
                        },
                    }
                );
                if (type === 'user') {
                    await this.client.invoke({
                        _: 'getChats',
                        offset_order: '9223372036854775807',
                        limit: 2147483647
                    });
                }
                const { id } = await this.getMe();
                this._id = id;
                if (this.afterUpdate) {
                    await this.afterUpdate();
                }
                return true;
            } catch (err) {
                this.dispose();
                throw err;
            }
        }
        return false;
    }

    protected readonly afterUpdate: () => Promise<any | void>;
    protected readonly onUpdate: (_: Update) => Promise<any | void>;

    getMe() {
        return this.client ? this.client.invoke({ _: 'getMe' }) : null;
    }

    dispose() {
        if (this.client) {
            this.client.destroy();
            this.client = null;
            this.phoneOrToken = null;
            this.apiHash = null;
            this.apiId = null;
            this._id = 0;
        }
    }

    async logout() {
        if (this.client) {
            await this.client.invoke({ _: 'logOut' });
        }
        this.dispose();
    }
}
