const { getOptions } = require('loader-utils');
const escapeRe = require('escape-string-regexp');

const r = `^\\w\\.'"#@`;

/**
 * @param {any} macros
 * @param {string} s
 */
function processMacros(macros, s) {
    return s.replace(
        new RegExp(`(^|[${r}\`])(${Object.keys(macros).join('|')})([${r}]|$)`, 'g'),
        (_, before, def, after) => before + macros[def] + after
    );
}

const cwd = new RegExp('^' + escapeRe(process.cwd().replace(/\\/g, '/') + '/'));

/**
 * @param {string} content
 */
module.exports = function (content) {
    //@ts-ignore
    if (this.cacheable) this.cacheable();
    //@ts-ignore
    const config = getOptions(this) || Object.create(null);
    config.macros = config.macros || Object.create(null);
    content = processMacros(config.macros, content);
    //@ts-ignore
    content = processMacros({ __FILE__: this.resourcePath.replace(/\\/g, '/').replace(cwd, './') }, content);
    // Replace __LINE__ by the line number
    if (content.includes('__LINE__')) {
        content = content
            .split('\n')
            .map((line, index) => processMacros({ __LINE__: index + 1 }, line))
            .join('\n');
    }
    return content;
};
//module.exports.raw = true;