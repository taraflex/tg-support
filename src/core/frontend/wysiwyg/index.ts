import 'vue-wysiwyg/dist/vueWysiwyg.css';
import './index.scss';

import Vue from 'vue';
try {
    Vue.use(require('vue-wysiwyg'), {
        hideModules: {
            separator: true,
            image: true
        }
    });
} catch{ }