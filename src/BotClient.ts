import {
    chat, Update$Input, updateNewCallbackQuery, updateNewMessage$Input, updateSupergroup, message$Input
} from 'tdl/types/tdlib';
import { Inject, Singleton } from 'typescript-ioc';

import { ClientSettings } from '@entities/ClientSettings';
import { Group, GroupRepository } from '@entities/Group';
import { Settings } from '@entities/Settings';
import { startCmdPayload } from '@utils/tg-utils';

import { AdminClient } from './AdminClient';
import { BaseTelegramClient } from './core/BaseTelegramClient';

@Singleton
export class BotClient extends BaseTelegramClient {
    protected system_chat_id: number = 0;
    constructor(
        @Inject protected readonly settings: ClientSettings,
        @Inject protected readonly admin: AdminClient,
        @Inject protected readonly grep: GroupRepository,
        @Inject protected readonly coreSettings: Settings,
    ) {
        super();
    }

    async selectChat(message: message$Input): Promise<{ invite_message_id?: number, group?: Group, chat: chat }> {
        let go = await this.grep.findOne(message.chat_id);
        if (go) {
            try {
                return { chat: await this.getSupergroupChat(go.supergroupId) };
            } catch (err) {
                LOG_INFO(message.chat_id, err);
                await this.grep.delete(go).catch(LOG_ERROR);
            }
        }
        const userChat = await this.client.invoke({ _: 'getChat', chat_id: message.chat_id });
        const { messages: [fmessage] } = await this.client.invoke({
            _: 'forwardMessages',
            chat_id: this.system_chat_id,
            from_chat_id: message.chat_id,
            message_ids: [message.id]
        });
        return this.admin.createGroup(userChat, this.id, fmessage.id);
    }

    protected readonly afterUpdate = async (): Promise<any> => {
        this.system_chat_id = 0;
        await this.admin.reinit((await this.getMe()).username);
        this.system_chat_id = (await this.getSupergroupChat(this.coreSettings.system_supergroup_id)).id
    }

    protected readonly activeTasks = new Set<string>();

    protected async checkChatInWorks(message: message$Input): Promise<any> {
        try {
            const info = await this.admin.getExtChatInfo(message.chat_id);
            if (info?.m) {
                LOG_INFO(info.m)
                await Promise.all([
                    this.admin.updateExtChatInfo(message.chat_id, info?.c),
                    this.deleteMessages(this.system_chat_id, ...info.m)
                ]);
            }
        } catch (err) {
            LOG_ERROR(err);
        }
    }

    protected readonly onUpdate = async (update: Update$Input): Promise<any> => {
        try {
            if (update._ === 'updateNewCallbackQuery') {
                const { payload, message_id } = update as updateNewCallbackQuery;
                if (payload._ === 'callbackQueryPayloadData') {
                    const data = Buffer.from(payload.data, 'base64').toString();
                    if (!this.activeTasks.has(data)) {
                        try {
                            this.activeTasks.add(data);
                            const [cmd, fmessage_id, supergroup_id] = JSON.parse(data);
                            switch (cmd) {
                                case 'd': return await Promise.all([
                                    this.admin.deleteSupergroup(supergroup_id),
                                    this.grep.delete({ supergroupId: supergroup_id }),
                                    this.deleteMessages(this.system_chat_id, fmessage_id, message_id),
                                ]);
                            }
                        } catch (err) {
                            LOG_ERROR(data, err);
                        } finally {
                            this.activeTasks.delete(data);
                        }
                    }
                }
            } else if (update._ === 'updateSupergroup') {
                const { supergroup: { id, member_count, } } = update as updateSupergroup;
                if (member_count === 0) {
                    await this.grep.delete(id);
                }
            } else if (update._ === 'updateNewMessage') {//todo add edit
                const { message } = update as updateNewMessage$Input;
                /*if (!message.can_be_forwarded && message.content._ === 'messageChatAddMembers') {
                    await this.checkChatInWorks(message);
                } else*/ if (
                    message.can_be_forwarded &&//system messages
                    !message.via_bot_user_id &&
                    !message.is_outgoing &&
                    !message.is_channel_post &&
                    this.system_chat_id !== message.chat_id //пропускаем сообщения из главной группы
                ) {
                    if (message.sender_user_id == message.chat_id) {//непосредственно чат с ботом
                        //@ts-ignore
                        const start = startCmdPayload(message.content?.text?.text)
                        if (start != null && start != 'noecho') {
                            return await this.sendMarkdown(message.chat_id, this.settings.startMessage);
                        } else if (message.chat_id != this.admin.id) {//если сообщение от обычного пользователя
                            const { chat: targetChat, group, invite_message_id } = await this.selectChat(message);
                            await this.client.invoke({
                                _: 'forwardMessages',
                                chat_id: targetChat.id,
                                from_chat_id: message.chat_id,
                                message_ids: [message.id]
                            });
                            if (group) {
                                const [link, title] = group.comment.split('|', 2);
                                const m = await this.sendMarkdown(
                                    this.system_chat_id,
                                    title,
                                    {
                                        _: 'replyMarkupInlineKeyboard',
                                        rows: [[{
                                            _: 'inlineKeyboardButton',
                                            text: 'Ответить',
                                            type: {
                                                _: 'inlineKeyboardButtonTypeUrl',
                                                url: link,
                                            },
                                        }, {
                                            _: 'inlineKeyboardButton',
                                            text: 'Игнорировать',
                                            type: {
                                                _: 'inlineKeyboardButtonTypeCallback',
                                                data: Buffer.from(JSON.stringify(['d', invite_message_id, group.supergroupId])).toString('base64'),
                                            },
                                        }]],
                                    });
                                await this.admin.updateExtChatInfo(targetChat.id, message.chat_id, [invite_message_id, m.id]);
                            }
                        }
                    } else {
                        await this.checkChatInWorks(message);
                        const chat_id = (await this.admin.getExtChatInfo(message.chat_id)).c;
                        //todo
                        /*
    | messageSticker$Input
    | messageVideoNote$Input
    | messageVoiceNote$Input
                        */
                        switch (message.content._) {
                            case 'messageText':
                                return await this.client.invoke({
                                    _: 'sendMessage',
                                    chat_id,
                                    input_message_content: {
                                        _: "inputMessageText",
                                        text: message.content.text,
                                        disable_web_page_preview: !message.content.web_page
                                    }
                                });
                            case 'messageVideo':
                                return await this.client.invoke({
                                    _: 'sendMessage',
                                    chat_id,
                                    input_message_content: {
                                        _: 'inputMessageVideo',
                                        video: {
                                            _: 'inputFileRemote',
                                            id: message.content.video.video.remote.id
                                        },
                                        thumbnail: {
                                            _: 'inputThumbnail',
                                            thumbnail: {
                                                _: 'inputFileRemote',
                                                id: message.content.video.thumbnail.photo.remote.id
                                            },
                                            width: message.content.video.thumbnail.width,
                                            height: message.content.video.thumbnail.height,
                                        },
                                        duration: message.content.video.duration,
                                        supports_streaming: message.content.video.supports_streaming,
                                        width: message.content.video.width,
                                        height: message.content.video.height,
                                    }
                                });
                            case 'messageAudio':
                                return await this.client.invoke({
                                    _: 'sendMessage',
                                    chat_id,
                                    input_message_content: {
                                        _: 'inputMessageAudio',
                                        audio: {
                                            _: 'inputFileRemote',
                                            id: message.content.audio.audio.remote.id
                                        },
                                        album_cover_thumbnail: {
                                            _: 'inputThumbnail',
                                            thumbnail: {
                                                _: 'inputFileRemote',
                                                id: message.content.audio.album_cover_thumbnail.photo.remote.id
                                            },
                                            width: message.content.audio.album_cover_thumbnail.width,
                                            height: message.content.audio.album_cover_thumbnail.height,
                                        },
                                        duration: message.content.audio.duration,
                                        title: message.content.audio.title,
                                        performer: message.content.audio.performer
                                    }
                                });
                            case 'messageAnimation':
                                return await this.client.invoke({
                                    _: 'sendMessage',
                                    chat_id,
                                    input_message_content: {
                                        _: 'inputMessageAnimation',
                                        animation: {
                                            _: 'inputFileRemote',
                                            id: message.content.animation.animation.remote.id
                                        },
                                        thumbnail: {
                                            _: 'inputThumbnail',
                                            thumbnail: {
                                                _: 'inputFileRemote',
                                                id: message.content.animation.thumbnail.photo.remote.id
                                            },
                                            width: message.content.animation.thumbnail.width,
                                            height: message.content.animation.thumbnail.height,
                                        },
                                        duration: message.content.animation.duration,
                                        width: message.content.animation.width,
                                        height: message.content.animation.height,
                                        caption: message.content.caption,
                                    }
                                });
                            case 'messageLocation':
                                return await this.client.invoke({
                                    _: 'sendMessage',
                                    chat_id,
                                    input_message_content: {
                                        _: 'inputMessageLocation',
                                        location: message.content.location,
                                    }
                                });
                            case 'messageVenue':
                                return await this.client.invoke({
                                    _: 'sendMessage',
                                    chat_id,
                                    input_message_content: {
                                        _: 'inputMessageVenue',
                                        venue: message.content.venue,
                                    }
                                });
                            case 'messageContact':
                                return await this.client.invoke({
                                    _: 'sendMessage',
                                    chat_id,
                                    input_message_content: {
                                        _: 'inputMessageContact',
                                        contact: message.content.contact,
                                    }
                                });
                            case 'messagePhoto':
                                return await this.client.invoke({
                                    _: 'sendMessage',
                                    chat_id,
                                    input_message_content: {
                                        _: 'inputMessagePhoto',
                                        photo: {
                                            _: 'inputFileRemote',
                                            id: message.content.photo.sizes[0].photo.remote.id
                                        },
                                        width: message.content.photo.sizes[0].width,
                                        height: message.content.photo.sizes[0].height,
                                        caption: message.content.caption
                                    }
                                });
                            case 'messageDocument':
                                return await this.client.invoke({
                                    _: 'sendMessage',
                                    chat_id,
                                    input_message_content: {
                                        _: 'inputMessageDocument',
                                        document: {
                                            _: 'inputFileRemote',
                                            id: message.content.document.document.remote.id,
                                        },
                                        thumbnail: {
                                            _: 'inputThumbnail',
                                            thumbnail: {
                                                _: 'inputFileRemote',
                                                id: message.content.document.thumbnail.photo.remote.id,
                                            },
                                            width: message.content.document.thumbnail.width,
                                            height: message.content.document.thumbnail.height,
                                        },
                                        caption: message.content.caption
                                    }
                                });
                            default:
                                return await this.sendMarkdown(message.chat_id, 'Неподдерживаемый тип сообщения.');
                        }
                    }
                }
            }
        } catch (err) {
            LOG_ERROR(err);
        }
    }
}