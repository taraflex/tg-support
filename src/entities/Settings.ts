import { Column, Repository } from 'typeorm';

import { SingletonEntity } from '@ioc';
import { Settings as CoreSettings } from '../core/entities/Settings';

@SingletonEntity()
export class Settings extends CoreSettings {
    @Column({ default: 0 })
    system_supergroup_id: number;
}

export abstract class SettingsRepository extends Repository<Settings>{ }