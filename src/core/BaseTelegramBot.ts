import { camelCase } from 'camel-case';
import { EventEmitter } from 'events';
import { Context } from 'koa';
import TelegramBot from 'node-telegram-bot-api';
import { paramCase } from 'param-case';
import rawBody from 'raw-body';
import { Inject } from 'typescript-ioc';
import { IDisposable } from 'xterm';

import { Settings } from '@entities/Settings';
import { MAX_PAYLOAD_SIZE } from '@middlewares/body-parse-msgpack';
import { BaseRequestOptions, get as getRequest, post as postRequest } from '@taraflex/http-client';
import { HOSTNAME } from '@utils/config';
import { rndString } from '@utils/rnd';
import { post } from '@utils/routes-helpers';
import { getBotInfo } from '@utils/tg-utils';

const TELEGRAM_ERRORS = new Set([400, 403, 404]);//todo may be remove 400 code or extendable filter

export function isCantWriteError(err: any) {
    return err && err.code === 'ETELEGRAM' &&
        err.response && err.response.body && TELEGRAM_ERRORS.has(err.response.body.error_code);
}

export abstract class BaseTelegramBot implements IDisposable {

    public bot: TelegramBot = null;
    protected lastWebHookToken: string;
    protected readonly allowedUpdates = new Set<keyof TelegramBot.Update>();
    protected lastRoot = '';
    public botInfo: TelegramBot.User;
    @Inject protected readonly settings: Settings;

    async dispose() {
        if (this.bot) {
            try {
                await getRequest(`https://api.telegram.org/bot${this.token}/deleteWebhook`);
            } catch (err) {
                LOG_ERROR(this.token, err);
            }
            (this.bot as EventEmitter).removeAllListeners();
            this.bot = null;
        }
    }

    async update(token: string) {
        const rootUrl = `https://${HOSTNAME}`;
        if (this.lastRoot !== rootUrl) {
            await this.dispose();
        }
        if (token && token === this.token) {
            return true;
        }
        const botInfo = await getBotInfo(token);

        await this.dispose();

        const { body } = await postRequest({
            url: `https://api.telegram.org/bot${token}/setWebhook`,
            formData: {
                url: rootUrl + `/hooks/process-${paramCase(this.constructor.name)}/` + (this.lastWebHookToken = await rndString()),
                allowed_updates: JSON.stringify(Array.from(this.allowedUpdates))
            }
        });
        if (JSON.parse(body).result !== true) {
            throw body.toString();
        }

        this.bot = new TelegramBot(token, { request: <any>BaseRequestOptions });

        this.allowedUpdates.forEach(update => {
            this.bot.on(<any>update, this[camelCase('on_' + update)].bind(this));
        });

        this.botInfo = botInfo;

        this.lastRoot = rootUrl;

        return true;
    }

    @post({ path: function () { return `/hooks/process-${paramCase(this.constructor.name)}/:token` } })
    async processTelegramMessages(ctx: Context) {
        if (ctx.params.token === this.lastWebHookToken && this.bot) {
            const updates = JSON.parse(await rawBody(ctx.req, { limit: MAX_PAYLOAD_SIZE, encoding: 'utf-8' }));
            this.bot.processUpdate(updates);
        }
        ctx.status = 200;
        ctx.body = '';
    }

    get token() {
        return (this.bot && (this.bot as any).token) || '';
    }
}