import { Column, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { SingletonEntity } from '@ioc';
import { API_HASH_RE } from '@utils/tg-utils';

@Access({ GET: 0, PATCH: 0 }, { display: 'single', icon: 'cog', order: 4 }, { PATCH: 'tgclient_save' })
@SingletonEntity()
export class ClientSettings {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ notEqual: 0, description: 'Telegram Api id взять [тут](https://my.telegram.org/apps)' })
    @Column({ type: 'int', default: 347322 })
    apiId: number;

    @v({ pattern: API_HASH_RE, description: 'Telegram Api hash взять [там же](https://my.telegram.org/apps)' })
    @Column({ default: '8301fdef6341ea326e378230babdcf97', length: 32 })
    apiHash: string;

    @v({ min: 5, description: 'Telegram телефон главного админа' })
    @Column({ default: '' })
    phone: string;

    @v({ min: 1, description: 'Токен телеграм бота (с ботом ранее должен быть начат диалог)' })
    @Column({ default: '' })
    token: string;

    @v({ min: 1, type: 'md-tgclient', description: 'Сообщение после отправки /start' })
    @Column({
        default: `Вас приветствует Горячая Линия Русской Инициативы
Подробнее о действиях РИ можно узнать тут [myfri.org](https://myfri.org/).

Быстро связаться можно по номерам ниже. 
По любым вопросам.
Италия: +3 980 014 82 03 (локально)
США: +1 844 919 11 44
Индонезия: +6 27 80 33 212 213
Таиланд: +66 18 000 13 767 (локально)
Франция: +33 805 223 161 (локально)
Греция: +3 588 004 137 39 (локально)` })
    startMessage: string;
}

export abstract class ClientSettingsRepository extends Repository<ClientSettings>{ }