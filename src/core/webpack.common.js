'use strict';

const merge = require('webpack-merge');
const config = require('./webpack.base');
const { resolve } = require('path');
const AssetsPlugin = require('assets-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const jEsc = require('js-string-escape');
const fs = require('fs');
const VirtualModulePlugin = require('virtual-module-webpack-plugin');

function getTypes() {
    //@ts-ignore
    const requires = Object.keys(require('../../package.json').dependencies).concat(["buffer", "querystring", "events", "http", "cluster", "worker_threads", "zlib", "os", "https", "punycode", "repl", "readline", "vm", "child_process", "url", "dns", "net", "dgram", "fs", "path", "string_decoder", "tls", "crypto", "stream", "util", "assert", "tty", "domain", "constants", "module", "process", "v8", "timers", "console", "async_hooks", "http2", "perf_hooks", "trace_events"]).map(m => `(id: "${m}"): typeof import("${m}");`).join('\n');

    const node = fs.readFileSync(__dirname + '/frontend/playground/node/index.d.ts.txt', 'utf-8').replace('/* tslint:disable-next-line:callable-types */', requires);
    return fs.readFileSync(__dirname + '/../../node_modules/monaco-editor/esm/vs/language/typescript/lib/lib.js', 'utf-8')
        .replace(/\/\*! \*{2,}.+?\*{2,} \*\/\\n\\n\\n\\n\/\/\/ /g, '')
        .replace(/lib_dom_dts\s=\s".+?[^\\]"/, `lib_dom_dts = "${jEsc(node)}"`)
        .replace(/(lib_webworker_importscripts_dts|lib_scripthost_dts)\s=\s".+?[^\\]"/g, '$1 = ""');
}

//@ts-ignore
module.exports = merge(config(true), {
    target: 'webworker',
    entry: {
        'common': './src/core/frontend/common.css',
        'editor.worker': 'monaco-editor/esm/vs/editor/editor.worker.js',
        'typescript.worker': 'monaco-editor/esm/vs/language/typescript/ts.worker'
    },
    plugins: [
        new VirtualModulePlugin({
            moduleName: 'node_modules/monaco-editor/esm/vs/language/typescript/lib/lib.js',
            contents: getTypes()
        }),
        new AssetsPlugin({
            fullPath: false,
            fileTypes: ['js'],
            includeAllFileTypes: false,
            filename: 'workers-assets.json',
            path: resolve(__dirname, '../../build'),
            entrypoints: true
        }),
        //@ts-ignore
        new CleanWebpackPlugin({
            cleanStaleWebpackAssets: false,
            cleanOnceBeforeBuildPatterns: [],
            cleanAfterEveryBuildPatterns: ['common.js*'],
            verbose: false
        })
    ]
});