import './index.scss';

const escDiv = document.createElement('div');
escDiv.hidden = true;

function escape(html: string, mode: string) {
    escDiv.appendChild(document.createTextNode(html));
    let text = escDiv.innerHTML;
    while (escDiv.lastChild) {
        escDiv.removeChild(escDiv.lastChild);
    }
    switch (mode) {
        case 'regex': return text;
        case 'md-tgclient': return text.replace(/\*\*([\s\S]+?)\*\*/g, '<b>**$1**</b>')
            .replace(/__([\s\S]+?)__/g, '<i>__$1__</i>')
            .replace(/~~([\s\S]+?)~~/g, '<span class="tgmd-strike">~~$1~~</span>')
            .replace(/```([\s\S]+?)```/g, '<span class="tgmd-code">```$1```</span>')
            .replace(/`([^\r\n`]+?)`/g, '<span class="tgmd-code">`$1`</span>')
            .replace(/\[([\s\S]+?)\]\(([\s\S]+?)\)/g, '<span class="tgmd-link">[$1]</span><span class="tgmd-link-src">($2)</span>') + '\n';
        case 'md-tgbotlegacy': return text.replace(/\*([\s\S]+?)\*/g, '<b>*$1*</b>')
            .replace(/_([\s\S]+?)_/g, '<i>_$1_</i>')
            .replace(/```([\s\S]+?)```/g, '<span class="tgmd-code">```$1```</span>')
            .replace(/`([^\r\n`]+?)`/g, '<span class="tgmd-code">`$1`</span>')
            .replace(/\[(.+?)\]\((.+?)\)/g, '<span class="tgmd-link">[$1]</span><span class="tgmd-link-src">($2)</span>') + '\n';
    }
}

export default {
    name: 'tgmd',
    props: {
        value: String,
        mode: String,
        readonly: Boolean
    },
    data() {
        return {
            text: '',
            html: ''
        }
    },
    mounted() {
        this.text = this.value || '';
        this.html = escape(this.value || '', this.mode)
    },
    watch: {
        value(v: string) {
            this.text = v || '';
        },
        text(v: string) {
            v = v || '';
            if (v != this.value) {
                this.html = escape(v, this.mode)
                this.$emit('input', v);
            }
        }
    }
}